--Question1
CREATE DATABASE question1 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE question1;

CREATE TABLE item_category(category_id int PRIMARY KEY AUTO_INCREMENT,
							category_name varchar(256) NOT NULL
							);
